public with sharing class SignatureHelper {
    
    @AuraEnabled
    public static void saveSign(String strSignElement,Id recId){

        ContentVersion cVersion = new ContentVersion();
        cVersion.ContentLocation = 'S'; 
        cVersion.PathOnClient = 'Signature-'+System.now() +'.png';
        cVersion.Origin = 'H';
        cVersion.Title = 'Signature-'+System.now() +'.png';
        cVersion.VersionData = EncodingUtil.base64Decode(strSignElement);
        Insert cVersion;
        
        Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
        
        ContentDocumentLink cDocLink = new ContentDocumentLink();
        cDocLink.ContentDocumentId = conDocument;
        cDocLink.LinkedEntityId = recId;
        cDocLink.ShareType = 'I';
        cDocLink.Visibility = 'AllUsers';
        Insert cDocLink;
    }
}